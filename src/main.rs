use chrono::{Datelike, Duration, NaiveDate, Utc};
use fern::colors::{Color, ColoredLevelConfig};
use log::info;
use serde_json::json;
use std::{
    fs::{self, File},
    io::{BufReader, Read},
    path::Path,
    str::FromStr,
};
use structopt::StructOpt;
use tcas_bot::{tar1090_url, tcas_csv_url, AdsbxBrowser, Incident, IncidentTracker, TcasRa};

#[derive(StructOpt, Debug)]
struct Args {
    #[structopt(long, default_value = "tcas-state.db")]
    db: String,
    #[structopt(
        long,
        value_name = "FILE",
        help = "Read RA data from file instead of a url."
    )]
    csv_file: Option<String>,
    #[structopt(
        long,
        help = "Date (UTC) to process, eg. '2021-03-31'. Defaults to current date if not specified.",
        conflicts_with = "csv_file"
    )]
    date: Option<NaiveDate>,
    #[structopt(long, help = "")]
    image_dir: Option<String>,
}

fn setup_logger() -> Result<(), fern::InitError> {
    let colors = ColoredLevelConfig::new().info(Color::Green);

    fern::Dispatch::new()
        .format(move |out, message, record| {
            out.finish(format_args!(
                "[{}] {}",
                colors.color(record.level()),
                // record.level(),
                message
            ))
        })
        .level(log::LevelFilter::Info)
        .chain(std::io::stderr())
        .apply()?;
    Ok(())
}

fn process_csv_url(options: &Args) {
    let client = reqwest::blocking::Client::builder()
        .user_agent("tcas_bot_john_wiseman / 0.1")
        .gzip(true)
        .build()
        .unwrap();
    let date = match options.date {
        Some(date) => date,
        None => Utc::now().naive_utc().date(),
    };
    let url = tcas_csv_url(&date);
    info!("Getting TCAS RA info from {}", url);
    let resp = client.get(&url).send().unwrap();
    let resp = resp.error_for_status().unwrap();
    let csv = resp.text().unwrap();
    process_csv_str(&csv, Some(&date), &options.image_dir);
}

fn process_csv_file(options: &Args) {
    let path = options.csv_file.as_ref().unwrap();
    let file = File::open(path).unwrap();
    let mut buffered_reader = BufReader::new(file);
    let mut contents = String::new();
    buffered_reader.read_to_string(&mut contents).unwrap();
    process_csv_str(&contents, None, &options.image_dir);
}

fn screenshot_basename(incident: &Incident) -> String {
    let ts = incident.earliest_timestamp();
    format!(
        "incident-{:04}{:02}{:02}-{:04}",
        ts.year(),
        ts.month(),
        ts.day(),
        incident.id
    )
}

fn screenshots(browser: &mut AdsbxBrowser, incident: &Incident) -> (Vec<u8>, Vec<u8>) {
    let ra_start_time = incident.earliest_timestamp();
    let ra_end_time = incident.latest_timestamp();
    let start_time = ra_start_time - Duration::seconds(20);
    let end_time = ra_end_time + Duration::seconds(20);
    let icaos = &incident.involved_hexes();
    let frame_0 = browser
        .screenshot(icaos, &start_time, &ra_start_time)
        .unwrap();
    let frame_1 = browser.screenshot(icaos, &start_time, &end_time).unwrap();
    (frame_0, frame_1)
}

fn process_csv_str(csv: &str, date: Option<&NaiveDate>, image_dir: &Option<String>) {
    // Add RAs to the tracker.
    let ras: Vec<TcasRa> = csv.lines().map(|l| TcasRa::from_str(l).unwrap()).collect();
    let mut tracker = IncidentTracker::new(None);
    for ra in ras {
        tracker.add_ra(&ra);
    }
    let incidents = tracker.incidents();

    // Write incidents as JSON.
    let json = incidents_as_json(&incidents, date);
    println!("{}", json);

    // Get screenshots of incidents if they don't already exist.
    let output_dir = match image_dir.as_ref() {
        None => Path::new("."),
        Some(dir) => Path::new(dir),
    };
    let mut browser = AdsbxBrowser::new().unwrap();
    let mut num_screenshots = 0;
    for i in incidents.iter() {
        let ss_basename = screenshot_basename(&i);
        let frame_0_path = output_dir.join(format!("{}-0.png", ss_basename));
        let frame_1_path = output_dir.join(format!("{}-1.png", ss_basename));
        if i.position().is_some() && !Path::new(&frame_0_path).exists() {
            info!(
                "Screenshotting incident {}/{}",
                i.id,
                incidents[incidents.len() - 1].id
            );
            let (frame_0, frame_1) = screenshots(&mut browser, &i);
            fs::write(&frame_0_path, frame_0).unwrap();
            fs::write(&frame_1_path, frame_1).unwrap();
            num_screenshots += 2;
            if num_screenshots >= 6 {
                browser = AdsbxBrowser::new().unwrap();
                num_screenshots = 0;
            }
        }
    }
}

fn incidents_as_json(incidents: &[Incident], date: Option<&NaiveDate>) -> String {
    let mut incident_jsons = vec![];
    let date = date.unwrap();
    for i in incidents {
        let start_ts = i.earliest_timestamp();
        let end_ts = i.latest_timestamp();
        let duration_s = (end_ts - start_ts).num_milliseconds() as f64 / 1000.0;
        let directives = i.dedup_directives();
        let msgs_json: Vec<serde_json::Value> = directives
            .iter()
            .map(|d| json!({ "ts": format!("{}", d.timestamp), "icao": d.hex, "msg": format!("{}", d.directive)}))
            .collect();
        let position: Option<serde_json::Value> = match i.position() {
            Some(pos) => Some(json!(Some(vec![pos.lon, pos.lat]))),
            _ => None,
        };
        let j = json!({
            "id": i.id,
            "date": format!("{:04}-{:02}-{:02}", date.year(), date.month(), date.day()),
            "icaos": i.involved_hexes(),
            "ts_start": format!("{}", start_ts),
            "ts_end": format!("{}", end_ts),
            "duration_s": duration_s,
            "adsbx_url": tar1090_url(&i.involved_hexes(), &i.earliest_timestamp(),&i.latest_timestamp()),
            "position": position,
            "screenshot_base": screenshot_basename(&i),
            "msgs": json!(msgs_json)
        });
        incident_jsons.push(j);
    }
    json!({
        "date": format!("{:04}-{:02}-{:02}", date.year(), date.month(), date.day()),
        "incidents":
        incident_jsons})
    .to_string()
}

fn main() {
    let args = Args::from_args();
    setup_logger().unwrap();
    if args.csv_file.is_some() {
        process_csv_file(&args);
    } else {
        process_csv_url(&args)
    }
}
