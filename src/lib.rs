use chrono::{prelude::*, ParseError};
use headless_chrome::{
    protocol::{page::ScreenshotFormat, target::methods::CreateTarget},
    Browser,
};
use itertools::Itertools;
use log::{debug, info};
use regex::Regex;
use rusqlite::NO_PARAMS;
use std::{
    collections::{HashMap, HashSet},
    fmt::{self, Display, Formatter},
    str::FromStr,
    sync::Arc,
    thread,
};
#[macro_use]
extern crate lazy_static;

#[derive(Debug, PartialEq, PartialOrd, Clone)]
pub struct Error(pub String);

#[derive(Debug, PartialEq, PartialOrd, Clone)]
pub struct TcasRa {
    pub timestamp: DateTime<Utc>,
    pub hex: String,
    pub position: Option<Pos>,
    pub instruction_text: String,
    pub incident_id: Option<u32>,
}

#[derive(Debug, PartialEq, PartialOrd, Clone)]
pub struct Pos {
    pub lat: f64,
    pub lon: f64,
}

#[derive(Debug, PartialEq, PartialOrd, Clone)]
pub enum RaDirective {
    ClearOfConflict,
    Conflict(Vec<String>),
}

#[derive(Debug, PartialEq, PartialOrd, Clone)]
pub struct SituatedDirective<'a> {
    pub timestamp: &'a DateTime<Utc>,
    pub hex: &'a String,
    pub directive: RaDirective,
}

#[derive(Debug, PartialEq, PartialOrd, Clone)]
pub struct RaInstruction {
    pub is_multithreat: bool,
    pub directive: RaDirective,
    pub target_hex: Option<String>,
}

#[derive(Debug, PartialEq, PartialOrd, Clone)]
pub struct Incident {
    pub id: u32,
    pub ras: Vec<TcasRa>,
}

#[derive(Debug, PartialEq, PartialOrd, Clone)]
pub enum RaStatus {
    NewIncident(u32),
    ExistingIncident(u32),
}

#[derive(Debug)]
pub struct IncidentTracker {
    conn: rusqlite::Connection,
    counter: u32,
}

pub struct AdsbxBrowser {
    // We keep a reference to the browser o it doesn't get dropped.
    #[allow(dead_code)]
    browser: Browser,
    tab: Arc<headless_chrome::Tab>,
    initialized: bool,
}

impl Display for Error {
    fn fmt(&self, f: &mut Formatter) -> fmt::Result {
        write!(f, "{}", self.0)
    }
}

impl From<ParseError> for Error
where
    ParseError: std::fmt::Debug,
{
    fn from(error: ParseError) -> Self {
        Error(format!("{:?}", error))
    }
}

impl From<failure::Error> for Error
where
    ParseError: std::fmt::Debug,
{
    fn from(error: failure::Error) -> Self {
        Error(format!("{:?}", error))
    }
}

impl std::error::Error for Error {}

impl FromStr for RaInstruction {
    type Err = Error;

    fn from_str(input: &str) -> Result<Self, Self::Err> {
        lazy_static! {
            static ref INSTRUCTION_RE: Regex =
                Regex::new(r"(?:(RA|RA Multithreat): )?(.+?)(?:TIDh: ([0-9a-fA-F]+))?$").unwrap();
        }
        let captures = match INSTRUCTION_RE.captures(input) {
            Some(captures) => captures,
            None => {
                return Err(Error(format!(
                    "Unable to parse RA instruction: '{}'",
                    input
                )))
            }
        };
        let is_multithreat = match captures.get(1) {
            Some(s) => s.as_str() == "RA multithreat",
            None => false,
        };
        let directive_str = captures
            .get(2)
            .ok_or_else(|| Error("BOO".to_string()))?
            .as_str()
            .trim();
        let directive = if directive_str.starts_with("Clear of Conflict") {
            RaDirective::ClearOfConflict
        } else {
            RaDirective::Conflict(
                directive_str
                    .split(';')
                    .map(|p| p.trim().to_string())
                    .filter(|p| !p.is_empty())
                    .collect(),
            )
        };
        let target_hex = captures.get(3).map(|g| g.as_str().to_string());
        Ok(RaInstruction {
            is_multithreat,
            directive,
            target_hex,
        })
    }
}

impl TcasRa {
    pub fn new_from_row(row: &rusqlite::Row) -> TcasRa {
        let ts_str: String = row.get("timestamp").unwrap();
        let lat: Option<f64> = row.get("lat").unwrap();
        let lon: Option<f64> = row.get("lon").unwrap();
        let position = match (lat, lon) {
            (Some(lat), Some(lon)) => Some(Pos { lat, lon }),
            _ => None,
        };
        TcasRa {
            timestamp: ts_str.parse::<DateTime<Utc>>().unwrap(),
            hex: row.get("hex").unwrap(),
            position,
            instruction_text: row.get("instruction").unwrap(),
            incident_id: row.get("incident_id").unwrap(),
        }
    }

    pub fn instruction(&self) -> Result<RaInstruction, Error> {
        RaInstruction::from_str(&self.instruction_text)
    }
}

// 2021-03-28,00:03:52.3, ad906f,DF:,17,bytes:,E2C20006B37168,  37.579061,-122.280698, 1100,ft, -832,fpm,ARA:,1100001,RAT:,0,MTE:,0,RAC:,0000,          , RA: Climb; TIDh: acdc5a,
impl FromStr for TcasRa {
    type Err = Error;

    // Parses a TcasRa from a string containing CSV.
    fn from_str(input: &str) -> Result<Self, Self::Err> {
        let pieces: Vec<&str> = input.split(',').collect();
        let timestamp_str = format!("{}T{}Z", pieces[0], pieces[1]);
        let timestamp = match timestamp_str.parse::<DateTime<Utc>>() {
            Ok(ts) => ts,
            Err(err) => {
                return Err(Error(format!(
                    "Unable to parse timestamp '{}': {}",
                    timestamp_str, err
                )))
            }
        };
        let hex = pieces[2].trim().to_string();
        let lat: Option<f64> = pieces[7].trim().parse().ok();
        let lon: Option<f64> = pieces[8].trim().parse().ok();
        let position = match (lat, lon) {
            (Some(lat), Some(lon)) => Some(Pos { lat, lon }),
            _ => None,
        };
        let instruction_text = pieces[22].trim().to_string();
        Ok(TcasRa {
            timestamp,
            hex,
            position,
            instruction_text,
            incident_id: None,
        })
    }
}

impl<'a> Incident {
    pub fn earliest_timestamp(&self) -> DateTime<Utc> {
        self.ras.iter().map(|ra| ra.timestamp).min().unwrap()
    }

    pub fn latest_timestamp(&self) -> DateTime<Utc> {
        self.ras.iter().map(|ra| ra.timestamp).max().unwrap()
    }

    pub fn involved_hexes(&self) -> Vec<String> {
        let subjects: HashSet<String> = self.ras.iter().map(|ra| ra.hex.clone()).collect();
        let targets: HashSet<String> = self
            .ras
            .iter()
            .filter_map(|ra| ra.instruction().unwrap().target_hex)
            .collect();
        let mut involved: Vec<String> = subjects.union(&targets).cloned().collect();
        involved.sort();
        involved
    }

    pub fn position(&self) -> Option<&Pos> {
        for ra in &self.ras {
            if let Some(pos) = &ra.position {
                return Some(pos);
            }
        }
        None
    }

    pub fn dedup_directives(&self) -> Vec<SituatedDirective> {
        let mut final_dirs = Vec::new();
        for (_, dirs) in &self
            .ras
            .iter()
            .map(|ra| SituatedDirective {
                hex: &ra.hex,
                timestamp: &ra.timestamp,
                directive: ra.instruction().unwrap().directive,
            })
            // Group by ICAO.
            .sorted_by_key(|sd| sd.hex)
            .group_by(|sd| sd.hex)
        {
            // For each group of messages from a single ICAO, de-dupe them.
            let mut hex_dirs: Vec<SituatedDirective> = dirs.collect();
            hex_dirs.sort_by_key(|sd| sd.timestamp);
            hex_dirs.dedup_by_key(|sd| sd.directive.clone());
            // Then add them to the list of messages from all aircraft.
            final_dirs.append(&mut hex_dirs);
        }
        // Sort the de-duped list of messages for all aircraft by timestamp.
        final_dirs.sort_by_key(|sd| sd.timestamp);
        final_dirs
    }
}

const SQL: &str = "DROP TABLE IF EXISTS incidents;

CREATE TABLE incidents (
       id INTEGER PRIMARY KEY ASC
       );

DROP TABLE IF EXISTS ras;

CREATE TABLE ras (
       timestamp TEXT,
       hex TEXT,
       lat REAL,
       lon REAL,
       instruction TEXT,
       target TEXT,
       directive TEXT,
       incident_id INTEGER,
       FOREIGN KEY(incident_id) REFERENCES incidents(id)
       );
";

impl IncidentTracker {
    pub fn new(db_path: Option<&String>) -> IncidentTracker {
        let conn = match db_path {
            Some(path) => rusqlite::Connection::open(path).unwrap(),
            None => rusqlite::Connection::open_in_memory().unwrap(),
        };
        conn.execute_batch(SQL).unwrap();
        IncidentTracker { conn, counter: 0 }
    }

    fn new_incident_id(&mut self) -> u32 {
        self.counter += 1;
        self.counter
    }

    fn most_recent_message_for_hex(&self, hex: &str) -> Option<TcasRa> {
        let mut stmt = self
            .conn
            .prepare(concat!(
                "SELECT * FROM ras WHERE hex = :hex OR target = :hex ",
                "ORDER BY timestamp DESC LIMIT 1"
            ))
            .unwrap();
        let mut rows = stmt
            .query_map_named(&[(":hex", &hex)], |row| Ok(TcasRa::new_from_row(row)))
            .unwrap();
        rows.next().map(|r| r.unwrap())
    }

    pub fn incidents(&self) -> Vec<Incident> {
        // Get all RAs from the database.
        let mut stmt = self
            .conn
            .prepare("SELECT * FROM ras ORDER BY timestamp")
            .unwrap();
        let mut rows = stmt.query(NO_PARAMS).unwrap();
        let mut ra_groups: HashMap<u32, Vec<TcasRa>> = HashMap::new();
        // Group RAs by incident ID. Rows isn't an iterator, or I think we could
        // do this more concisely with map and group_by.
        while let Some(row) = rows.next().unwrap() {
            let incident_id = row.get("incident_id").unwrap();
            let ras = ra_groups.entry(incident_id).or_insert_with(Vec::new);
            ras.push(TcasRa::new_from_row(row));
        }
        // Build incidents from the RAs
        let mut incidents: Vec<Incident> = ra_groups
            .iter()
            .map(|(incident_id, ras)| Incident {
                id: *incident_id,
                ras: ras.clone(),
            })
            .collect();
        incidents.sort_by_key(|i| i.id);
        incidents
    }

    pub fn add_ra(&mut self, ra: &TcasRa) -> RaStatus {
        let opt_prev_ra = self.most_recent_message_for_hex(&ra.hex);
        let status = match opt_prev_ra {
            Some(prev_ra) => {
                let delta = ra.timestamp - prev_ra.timestamp;
                if delta.num_seconds() > 300 {
                    let incident_id = self.new_incident_id();
                    info!(
                        "Creating new incident {} for {} at {}: {:?}",
                        incident_id, ra.hex, ra.timestamp, ra.instruction_text
                    );
                    RaStatus::NewIncident(incident_id)
                } else {
                    debug!(
                        "New message for existing incident {}: {}",
                        prev_ra.incident_id.unwrap(),
                        ra.instruction_text
                    );
                    RaStatus::ExistingIncident(prev_ra.incident_id.unwrap())
                }
            }
            None => {
                let incident_id = self.new_incident_id();
                info!(
                    "Creating new incident {} for {} at {}: {:?}",
                    incident_id, ra.hex, ra.timestamp, ra.instruction_text
                );
                RaStatus::NewIncident(incident_id)
            }
        };
        let lat = ra.position.as_ref().map(|p| p.lat);
        let lon = ra.position.as_ref().map(|p| p.lon);
        let ins = ra.instruction().unwrap();
        let directive = format!("{:?}", ins.directive);
        self.conn
            .execute(
                "INSERT INTO ras
              (timestamp, hex, lat, lon, instruction, target, directive, incident_id)
              VALUES (?1, ?2, ?3, ?4, ?5, ?6, ?7, ?8);",
                rusqlite::params![
                    format!("{}", ra.timestamp),
                    ra.hex,
                    lat,
                    lon,
                    ra.instruction_text,
                    ins.target_hex,
                    directive,
                    match status {
                        RaStatus::NewIncident(id) => id,
                        RaStatus::ExistingIncident(id) => id,
                    }
                ],
            )
            .unwrap();
        status
    }
}

impl fmt::Display for RaDirective {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            RaDirective::ClearOfConflict => write!(f, "Clear of conflict"),
            RaDirective::Conflict(cmds) => write!(f, "{}", cmds.join(", ")),
        }
    }
}

impl AdsbxBrowser {
    pub fn new() -> std::result::Result<AdsbxBrowser, Error> {
        let browser = Browser::default()?;
        let tab = browser
            .new_tab_with_options(CreateTarget {
                url: "chrome://version",
                width: Some(1600),
                height: Some(800),
                browser_context_id: None,
                enable_begin_frame_control: None,
            })
            .unwrap();
        Ok(AdsbxBrowser {
            browser,
            tab,
            initialized: false,
        })
    }

    pub fn screenshot(
        &mut self,
        icaos: &[String],
        start_time: &DateTime<Utc>,
        end_time: &DateTime<Utc>,
    ) -> Result<Vec<u8>, Error> {
        let url = &tar1090_url(icaos, start_time, end_time);
        let long_delay = std::time::Duration::from_millis(4000);
        let short_delay = std::time::Duration::from_millis(100);
        self.tab.navigate_to(url)?;
        self.tab.wait_until_navigated()?;
        thread::sleep(long_delay);
        self.tab
            .wait_for_element("#selected_infoblock")?
            .call_js_fn("function() { this.remove(); }", false)?;
        thread::sleep(short_delay);
        // Only need to turn on these labels once per browser; their state is
        // stored by the site in cookies.
        if !self.initialized {
            // Show timestamps, altitudes and speeds along each track.
            self.tab.press_key("k")?;
            thread::sleep(short_delay);
            // Show aircraft registration labels.
            self.tab.press_key("l")?;
            thread::sleep(short_delay);
            self.initialized = true;
        }
        #[allow(unused_must_use)]
        if let Ok(close) = self.tab.wait_for_element("[Title=\"Close\"]") {
            close.click();
            thread::sleep(short_delay);
        }
        let viewport = self
            .tab
            .wait_for_element("#map_container")?
            .get_box_model()?
            .margin_viewport();
        let image_data =
            self.tab
                .capture_screenshot(ScreenshotFormat::PNG, Some(viewport), true)?;
        Ok(image_data)
    }
}

// https://globe.adsbexchange.com/?icao=a05277&zoom=11.6&showTrace=2021-03-28&startTime=16:11&endTime=16:12
pub fn tar1090_url(
    icaos: &[String],
    start_time: &DateTime<Utc>,
    end_time: &DateTime<Utc>,
) -> String {
    let ts = |t: &DateTime<Utc>| format!("{:02}:{:02}:{:02}", t.hour(), t.minute(), t.second());
    let icaos_str = icaos.join(",");
    let date = format!(
        "{}-{:02}-{:02}",
        start_time.year(),
        start_time.month(),
        start_time.day()
    );
    format!(
        "https://globe.adsbexchange.com/?icao={}&zoom=11.6&showTrace={}&startTime={}&endTime={}",
        icaos_str,
        date,
        ts(start_time),
        ts(end_time)
    )
}

// https://globe.adsbexchange.com/globe_history/2021/03/28/acas/acas.csv

pub fn tcas_csv_url(date: &NaiveDate) -> String {
    format!(
        "https://globe.adsbexchange.com/globe_history/{:04}/{:02}/{:02}/acas/acas.csv",
        date.year(),
        date.month(),
        date.day()
    )
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_tcas_csv_url() {
        assert_eq!(
            tcas_csv_url(&"2021-03-28".parse::<NaiveDate>().unwrap()),
            "https://globe.adsbexchange.com/globe_history/2021/03/28/acas/acas.csv"
        );
    }

    #[test]
    fn test_tcas_ra_parse() {
        let ra = TcasRa::from_str(concat!(
            "2021-03-28,00:03:52.3, ad906f,DF:,17,bytes:,E2C20006B37168,",
            "  37.579061,-122.280698, 1100,ft, -832,fpm,ARA:,1100001,RAT:",
            ",0,MTE:,0,RAC:,0000,          , RA: Climb; TIDh: acdc5a,"
        ))
        .unwrap();
        assert_eq!(
            ra,
            TcasRa {
                timestamp: "2021-03-28T00:03:52.300Z".parse::<DateTime<Utc>>().unwrap(),
                hex: "ad906f".to_string(),
                position: Some(Pos {
                    lat: 37.579061,
                    lon: -122.280698
                }),
                instruction_text: "RA: Climb; TIDh: acdc5a".to_string(),
                incident_id: None
            }
        );
        assert_eq!(
            ra.instruction().unwrap(),
            RaInstruction {
                is_multithreat: false,
                target_hex: Some("acdc5a".to_string()),
                directive: RaDirective::Conflict(vec!["Climb".to_string()])
            }
        );

        let ra = TcasRa::from_str(concat!(
            "2021-03-28,00:04:33.2, ad906f,DF:,17,bytes:,E2A00126B37168,  ",
            "37.591677,-122.311388,  575,ft, -576,fpm,ARA:,1010000,RAT:,1,MTE:,",
            "0,RAC:,0100, not above, Clear of Conflict; TIDh: acdc5a,"
        ))
        .unwrap();
        assert_eq!(
            ra,
            TcasRa {
                timestamp: "2021-03-28T00:04:33.200Z".parse::<DateTime<Utc>>().unwrap(),
                hex: "ad906f".to_string(),
                position: Some(Pos {
                    lat: 37.591677,
                    lon: -122.311388
                }),
                instruction_text: "Clear of Conflict; TIDh: acdc5a".to_string(),
                incident_id: None
            }
        );
        assert_eq!(
            ra.instruction().unwrap(),
            RaInstruction {
                is_multithreat: false,
                target_hex: Some("acdc5a".to_string()),
                directive: RaDirective::ClearOfConflict
            }
        );
    }

    #[test]
    fn test_incident_tracker_incidents() {
        let ts = |s: &str| s.parse::<DateTime<Utc>>().unwrap();
        let mut tracker = IncidentTracker::new(None);
        let ra1 = TcasRa {
            timestamp: ts("2021-04-01T00:00:00Z"),
            hex: "AAAAAA".to_string(),
            position: None,
            instruction_text: "RA: Climb; TIDh: BBBBBB".to_string(),
            incident_id: None,
        };
        assert_eq!(RaStatus::NewIncident(1), tracker.add_ra(&ra1));
        let ra2 = TcasRa {
            hex: "AAAAAA".to_string(),
            position: None,
            timestamp: ts("2021-04-01T00:01:00Z"),
            instruction_text: "Clear of Conflict; TIDh: CCCCCC".to_string(),
            incident_id: None,
        };
        assert_eq!(RaStatus::ExistingIncident(1), tracker.add_ra(&ra2));
        let ra3 = TcasRa {
            hex: "AAAAAA".to_string(),
            position: None,
            timestamp: ts("2021-04-01T01:00:00Z"),
            instruction_text: "RA: Climb; TIDh: CCCCCC".to_string(),
            incident_id: None,
        };
        assert_eq!(RaStatus::NewIncident(2), tracker.add_ra(&ra3));
        let incidents = tracker.incidents();
        assert_eq!(incidents[0].id, 1);
        assert_eq!(
            incidents[0].ras[0],
            TcasRa {
                timestamp: ts("2021-04-01T00:00:00Z"),
                hex: "AAAAAA".to_string(),
                position: None,
                instruction_text: "RA: Climb; TIDh: BBBBBB".to_string(),
                incident_id: Some(1,),
            }
        );
        assert_eq!(
            incidents[0].ras[1],
            TcasRa {
                timestamp: ts("2021-04-01T00:01:00Z"),
                hex: "AAAAAA".to_string(),
                position: None,
                instruction_text: "Clear of Conflict; TIDh: CCCCCC".to_string(),
                incident_id: Some(1,),
            }
        );
        assert_eq!(
            incidents[0].earliest_timestamp(),
            ts("2021-04-01T00:00:00Z")
        );
        assert_eq!(incidents[0].latest_timestamp(), ts("2021-04-01T00:01:00Z"));
        assert_eq!(
            incidents[0].involved_hexes(),
            vec![
                "AAAAAA".to_string(),
                "BBBBBB".to_string(),
                "CCCCCC".to_string()
            ]
        );
    }

    #[test]
    fn test_incident_tracker_dedup_directives() {
        let ts = |s: &str| s.parse::<DateTime<Utc>>().unwrap();
        let mut tracker = IncidentTracker::new(None);
        let ra1 = TcasRa {
            timestamp: ts("2021-04-01T00:00:00Z"),
            hex: "AAAAAA".to_string(),
            position: None,
            instruction_text: "RA: Climb; TIDh: BBBBBB".to_string(),
            incident_id: None,
        };
        assert_eq!(RaStatus::NewIncident(1), tracker.add_ra(&ra1));
        let ra2 = TcasRa {
            timestamp: ts("2021-04-01T00:00:01Z"),
            hex: "AAAAAA".to_string(),
            position: None,
            instruction_text: "RA: Climb; TIDh: BBBBBB".to_string(),
            incident_id: None,
        };
        assert_eq!(RaStatus::ExistingIncident(1), tracker.add_ra(&ra2));
        let ra3 = TcasRa {
            timestamp: ts("2021-04-01T00:00:02Z"),
            hex: "BBBBBB".to_string(),
            position: None,
            instruction_text: "RA: Climb; TIDh: BBBBBB".to_string(),
            incident_id: None,
        };
        assert_eq!(RaStatus::ExistingIncident(1), tracker.add_ra(&ra3));
        let ra4 = TcasRa {
            timestamp: ts("2021-04-01T00:00:03Z"),
            hex: "AAAAAA".to_string(),
            position: None,
            instruction_text: "RA: Climb; TIDh: BBBBBB".to_string(),
            incident_id: None,
        };
        assert_eq!(RaStatus::ExistingIncident(1), tracker.add_ra(&ra4));
        let ra5 = TcasRa {
            timestamp: ts("2021-04-01T00:00:04Z"),
            hex: "BBBBBB".to_string(),
            position: None,
            instruction_text: "RA: Descend; TIDh: BBBBBB".to_string(),
            incident_id: None,
        };
        assert_eq!(RaStatus::ExistingIncident(1), tracker.add_ra(&ra5));
        let incident = &tracker.incidents()[0];
        let deduped = incident.dedup_directives();
        assert_eq!(
            deduped[0],
            SituatedDirective {
                timestamp: &ra1.timestamp,
                hex: &ra1.hex,
                directive: RaDirective::Conflict(vec!["Climb".to_string()])
            }
        );
        assert_eq!(
            deduped[1],
            SituatedDirective {
                timestamp: &ra3.timestamp,
                hex: &ra3.hex,
                directive: RaDirective::Conflict(vec!["Climb".to_string()])
            }
        );
        assert_eq!(
            deduped[2],
            SituatedDirective {
                timestamp: &ra5.timestamp,
                hex: &ra5.hex,
                directive: RaDirective::Conflict(vec!["Descend".to_string()])
            }
        );
    }
}
