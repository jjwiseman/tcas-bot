<a href="https://gitlab.com/jjwiseman/tcas-bot/-/pipelines">
<img alt="pipeline status"
     src="https://gitlab.com/jjwiseman/tcas-bot/badges/master/pipeline.svg">
</a>

# TCAS bot

Currently generates a static site that catalogs and visualizes Traffic
Collisions Avoidance System Resolution Advisories (TCAS RAs, or "collision
avoidance") messages from aircraft received by the ADS-B Exchange network.
