const Image = require("@11ty/eleventy-img");
const path = require("path");

function imageFilename(id, src, width, format, options) {
    const extension = path.extname(src);
    const name = path.basename(src, extension);
    return `${name}-${width}w.${format}`;
}

async function thumbnailShortcode(src, alt, size) {
    console.log("Creating thumbnail " + src);
    let metadata = await Image(src, {
        widths: [480],
        formats: ["webp"],
        urlPath: "../images/",
        outputDir: "_site/images",
        filenameFormat: imageFilename,
        sharpWebpOptions: { quality: 80 }
    });

    // let imageAttributes = {
    //     alt,
    //     sizes,
    //     loading: "lazy",    
    //     decoding: "async",
    //     width: null,
    //     height: null
    // };

    // return `<img src="${metadata.url}" alt="${alt}" loading="lazy" decoding="async">`;
    // return Image.generateHTML(metadata, imageAttributes);
    let data = metadata.webp[size];
    return `<img src="${data.url}" width="${data.width}" height="${data.height}" alt="${alt}" loading="lazy" decoding="async">`;
}

async function imageShortcode(src, alt, size) {
    console.log("Creating image " + src);
    let metadata = await Image(src, {
        widths: [480, 1200],
        formats: ["webp"],
        urlPath: "../images/",
        outputDir: "_site/images",
        filenameFormat: imageFilename,
        sharpWebpOptions: { quality: 90 }
    });

    // let imageAttributes = {
    //     alt,
    //     sizes,
    //     loading: "lazy",
    //     decoding: "async",
    //     width: null,
    //     height: null
    // };

    // return `<img src="${metadata.url}" alt="${alt}" loading="lazy" decoding="async">`;
    // return Image.generateHTML(metadata, imageAttributes);
    let data = metadata.webp[size];
    return `<img src="${data.url}" width="${data.width}" height="${data.height}" alt="${alt}" loading="lazy" decoding="async">`;
}

module.exports = function (eleventyConfig) {
    // Output directory: _site

    eleventyConfig.addNunjucksAsyncShortcode("thumbnail", thumbnailShortcode);
    eleventyConfig.addLiquidShortcode("thumbnail", thumbnailShortcode);
    eleventyConfig.addJavaScriptFunction("thumbnail", thumbnailShortcode);

    eleventyConfig.addNunjucksAsyncShortcode("image", imageShortcode);
    eleventyConfig.addLiquidShortcode("image", imageShortcode);
    eleventyConfig.addJavaScriptFunction("image", imageShortcode);

    // Copy `images/` to `_site/images`
    //eleventyConfig.addPassthroughCopy("images");

    //   // Copy `css/fonts/` to `_site/css/fonts`
    //   // If you use a subdirectory, it’ll copy using the same directory structure.
    //   eleventyConfig.addPassthroughCopy("css/fonts");
};
