use std::{
    fs::File,
    io::{BufReader, Read},
    str::FromStr,
};
use tcas_bot::{IncidentTracker, TcasRa};

fn main() {
    let args: Vec<String> = std::env::args().collect();
    let file = File::open(args[1].clone()).unwrap();
    let mut buffered_reader = BufReader::new(file);
    let mut contents = String::new();
    buffered_reader.read_to_string(&mut contents).unwrap();
    let records: Vec<TcasRa> = contents
        .lines()
        .map(|l| TcasRa::from_str(l).unwrap())
        .collect();
    let mut tracker = IncidentTracker::new(None);
    for ra in records.iter() {
        println!("{:?}", ra);
        println!("{:?}", ra.instruction());
        tracker.add_ra(ra);
    }
}
