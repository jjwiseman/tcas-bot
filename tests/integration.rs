use std::str::FromStr;
use tcas_bot::{IncidentTracker, TcasRa};

#[test]
fn test_parsing_whole_file() {
    let csv_str = include_str!("acas.csv");
    let records: Vec<TcasRa> = csv_str
        .lines()
        .map(|l| TcasRa::from_str(l).unwrap())
        .collect();
    let mut tracker = IncidentTracker::new(None);
    for ra in records.iter() {
        println!("{:?}", ra);
        println!("{:?}", ra.instruction());
        tracker.add_ra(ra);
    }
    assert_eq!(records.len(), 4656);
}
